# Docker Base

This repository houses Dockerfiles and docker-compose multi-container configurations for developing web applications.

All the images contain a webserver (Apache/Nginx), MySQL and phpMyAdmin.

To test, inside one of the folders (LAMP/LEMP), execute the command:

`docker-compose up -d`

You will be able to access the webserver at localhost in your browser. To shut down the container, execute the following command in the __same folder__ where the ```docker-compose up -d``` command was executed:

`docker-compose down`

Modify the docker-composer files suitably to use different PHP versions as required.

To use with an existing project, copy the docker-compose.yml and the corresponding Dockerfile referenced in the docker-compose.yml file to the root of your project directory. Update the docker-compose.yml file with volume paths, enviroment variables etc suitable to the project configuration and launch docker!