FROM php:7.0-fpm
RUN apt-get update \
    && apt-get install -y  \
    git \
    libzip-dev \
    zip \
    unzip \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install mysqli pdo_mysql