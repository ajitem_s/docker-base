FROM nginx:1.12
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    gnupg \
    wget \
    apt-transport-https
RUN wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add - && \
    echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-6.x.list && \
    apt-get update && \
    apt-get install -y \
    filebeat && \
    update-rc.d filebeat defaults 95 10

